package com.mobimeo.giphy.repository

import com.mobimeo.giphy.BaseTest
import com.mobimeo.giphy.BuildConfig
import com.mobimeo.giphy.TestSchedulerProvider
import com.mobimeo.giphy.data.defaultGiphyResponse
import com.mobimeo.giphy.network.api.GiphyApi
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Before
import org.junit.Test

class GiphyRepositoryTest: BaseTest() {

    val giphyApi: GiphyApi = mock {
        on { getGifs(any(), any(), any()) } doReturn Single.just(defaultGiphyResponse)
    }

    lateinit var tested: GiphyRepository

    @Before
    fun setUp() {
        tested = GiphyRepository(giphyApi, TestSchedulerProvider())
    }

    @Test
    fun `When get gifs success, Then fetch data`() {
        val testObserver = tested.getGifs(10, 25).test()

        assertThat(testObserver.values().single(), `is`(defaultGiphyResponse))
        verify(giphyApi).getGifs(BuildConfig.API_KEY, 10, 25)
    }
}