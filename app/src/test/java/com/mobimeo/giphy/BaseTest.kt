package com.mobimeo.giphy

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.ClassRule

open class BaseTest {

    companion object {
        @ClassRule
        @JvmField
        val rule = InstantTaskExecutorRule()

        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }
}
