package com.mobimeo.giphy.model

import com.mobimeo.giphy.feature.giphy_list.GiphyListUseCaseType
import com.mobimeo.giphy.feature.giphy_list.ImageGif
import io.reactivex.Single
import io.reactivex.subjects.SingleSubject

class DummyGiphyListUseCase: GiphyListUseCaseType {

    val subject = SingleSubject.create<List<ImageGif>>()

    override fun getGifs(): Single<List<ImageGif>> = subject
}