package com.mobimeo.giphy.data

import com.mobimeo.giphy.feature.giphy_list.ImageGif

val defaultImageGif = ImageGif(id = "", fixedUrl = "url", originalUrl = "originalUrl")
val defaultListImageGif = listOf(
    defaultImageGif,
    defaultImageGif.copy(id = "id2"),
    defaultImageGif.copy(id = "id3"),
    defaultImageGif.copy(id = "id4"),
    defaultImageGif.copy(id = "id5"),
    defaultImageGif.copy(id = "id6"),
    defaultImageGif.copy(id = "id7"),
    defaultImageGif.copy(id = "id8"))