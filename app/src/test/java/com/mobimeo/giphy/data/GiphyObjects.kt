package com.mobimeo.giphy.data

import com.mobimeo.giphy.repository.GiphyReponse

val defaultMetaData = GiphyReponse.MetaData(
    status = 200,
    msg = "OK",
    responseId = "5c47185d526b654d737f0375"
)

val defaultInfo = GiphyReponse.Info(
    totalCount = 20000,
    count = 25,
    offset = 0
)

val defaultImages = GiphyReponse.Data.Images(
    fixWidth = GiphyReponse.Data.Images.Image("https://bestGif.gif"),
    original = GiphyReponse.Data.Images.Image("https://bestGifOriginal.gif")
)

val defaultData = GiphyReponse.Data(
    type = "gif",
    id = "id",
    images = defaultImages
)

val defaultGiphyResponse = GiphyReponse(
    data = listOf(defaultData),
    info = defaultInfo,
    meta = defaultMetaData
)