package com.mobimeo.giphy.feature.giphy_list

import com.mobimeo.giphy.BaseTest
import com.mobimeo.giphy.data.defaultData
import com.mobimeo.giphy.data.defaultGiphyResponse
import com.mobimeo.giphy.data.defaultImages
import com.mobimeo.giphy.repository.GiphyRepositoryType
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is
import org.junit.Before
import org.junit.Test

class GiphyListViewUseCaseTest: BaseTest() {

    val giphyRepository: GiphyRepositoryType = mock {
        on { getGifs(any(), any()) } doReturn Single.just(defaultGiphyResponse)
    }


    lateinit var tested: GiphyListUseCase

    @Before
    fun setUp() {
        tested = GiphyListUseCase(giphyRepository)
    }


    @Test
    fun `When first time get gifs success, Then fetch data`() {
        val testObserver = tested.getGifs().test()

        assertThat(
            testObserver.values().single(),
            Is.`is`(listOf(ImageGif(defaultData.id, defaultImages.fixWidth.url, defaultImages.original.url)))
        )
        verify(giphyRepository).getGifs(offset = 0, limit = GiphyListUseCase.ITEMS_PAGE)
    }

    @Test
    fun `When second time get gifs success, Then update offset`() {
        tested.getGifs().test()
        val testObserver = tested.getGifs().test()

        assertThat(
            testObserver.values().single(),
            Is.`is`(listOf(ImageGif(defaultData.id, defaultImages.fixWidth.url, defaultImages.original.url)))
        )
        verify(giphyRepository).getGifs(offset = 25, limit = GiphyListUseCase.ITEMS_PAGE)
    }

    @Test
    fun `When there are not more items to fetch, Then return empty list`() {
        tested.setCurrentItems((GiphyListUseCase.ITEMS_PAGE * GiphyListUseCase.MAX_PAGE) + 1)
        val testObserver = tested.getGifs().test()

        assertThat(testObserver.values().single(), Is.`is`(emptyList()))
        verifyZeroInteractions(giphyRepository)
    }
}