package com.mobimeo.giphy.feature.giphy_list

import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import com.mobimeo.giphy.BaseTest
import com.mobimeo.giphy.R
import com.mobimeo.giphy.TestSchedulerProvider
import com.mobimeo.giphy.data.defaultListImageGif
import com.mobimeo.giphy.model.DummyGiphyListUseCase
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.atLeastOnce
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class GiphyListViewModelTest: BaseTest() {

    val observer = mock<Observer<GiphyListData>>()
    val singleObserver = mock<Observer<@StringRes Int>>()
    val actions = mock<GiphyListActions>()
    val useCase = DummyGiphyListUseCase()

    lateinit var tested: GiphyListViewModel

    @Before
    fun setUp() {
        tested = GiphyListViewModel(useCase, TestSchedulerProvider(), actions)
        tested.observeViewDataForever(observer)
        tested.observeSingleDataForever(singleObserver)
    }

    @Test
    fun `while waiting for results, provide LoadingData`() {
        assertThat(currentState(), instanceOf(GiphyListData.Loading::class.java))
    }

    @Test
    fun `When gif is load, provide initial gifs without single data`() {
        publishData()

        val data = tested.viewDataValue() as GiphyListData.Gif
        assertThat(data.gifs.size, `is`(8))
        assertThat(data.gifs.contains(GiphyRow.Loading()), `is`(false))
        assertThat(data.enableEndLessScroll, `is`(true))
        checkSingleData(null)
    }

    @Test
    fun `When gif fails, provide proper error data and single data`() {
        publishErrorData()

        val data = tested.viewDataValue() as GiphyListData.Gif
        assertThat(data.gifs.size, `is`(0))
        assertThat(data.gifs.contains(GiphyRow.Loading()), `is`(false))
        checkSingleData(R.string.general_error)
    }

    @Test
    fun `When try to get more gifs, display loading row`() {
        publishData()

        tested.requestGifs()

        argumentCaptor<GiphyListData>().apply {
            verify(observer, atLeastOnce()).onChanged(capture())

            val beforeLastValue =  allValues[allValues.count() - 2] as GiphyListData.Gif
            assertThat(
                beforeLastValue.gifs.last(),
                instanceOf(GiphyRow.Loading::class.java)
            )
           assertThat(beforeLastValue.enableEndLessScroll, `is`(false))
        }
    }

    @Test
    fun `When a gif ic clicked, display gif screen with the right url`() {
        tested.onGiphyClicked("bestUrl")

        verify(actions).onGiphyClicked("bestUrl")
    }

    fun publishData(photos: List<ImageGif> = defaultListImageGif) {
        useCase.subject.onSuccess(photos)
    }

    fun publishErrorData() {
        useCase.subject.onError(Throwable())
    }

    fun checkSingleData(@StringRes resId: Int?) {
        MatcherAssert.assertThat(tested.viewSingleValue(), CoreMatchers.`is`(resId))
    }

    fun currentState() = tested.viewDataValue()
}