package com.mobimeo.giphy

import com.mobimeo.giphy.utils.SchedulerProvider
import io.reactivex.schedulers.Schedulers

class TestSchedulerProvider: SchedulerProvider {
    override fun ui() = Schedulers.trampoline()

    override fun computation() = Schedulers.trampoline()

    override fun io() = Schedulers.trampoline()
}