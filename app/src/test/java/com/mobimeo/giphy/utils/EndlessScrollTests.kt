package com.mobimeo.giphy.utils

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobimeo.giphy.utils.EndlessScroll
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Test

class EndlessScrollTests {

    val recyclerView: RecyclerView = mock()
    val gridLayoutManager: GridLayoutManager = mock()
    val function: () -> Unit = mock()

    val tested: EndlessScroll = EndlessScroll(gridLayoutManager, function)

    @Test
    fun `When scroll is less than 5 positions to the end, invoke function`() {
        whenever(gridLayoutManager.childCount).thenReturn(10)
        whenever(gridLayoutManager.findFirstVisibleItemPosition()).thenReturn(5)
        whenever(gridLayoutManager.itemCount).thenReturn(18)

        tested.onScrolled(recyclerView, 0, 0)

        verify(function).invoke()
    }

    @Test
    fun `When scroll is more than 5 positions to the end, don't invoke function`() {
        whenever(gridLayoutManager.childCount).thenReturn(10)
        whenever(gridLayoutManager.findFirstVisibleItemPosition()).thenReturn(5)
        whenever(gridLayoutManager.itemCount).thenReturn(21)

        tested.onScrolled(recyclerView, 0, 0)

        verify(function, never()).invoke()
    }

    @Test
    fun `When scroll is less than 5 positions to the end but is disable, don't invoke function`() {
        whenever(gridLayoutManager.childCount).thenReturn(10)
        whenever(gridLayoutManager.findFirstVisibleItemPosition()).thenReturn(5)
        whenever(gridLayoutManager.itemCount).thenReturn(18)

        tested.enable(false)
        tested.onScrolled(recyclerView, 0, 0)

        verify(function, never()).invoke()
    }
}