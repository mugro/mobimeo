package com.mobimeo.giphy.utils

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import com.mobimeo.giphy.BaseTest
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class SingleLiveDataTests: BaseTest() {

    // The class that has the lifecycle
    private val mOwner: LifecycleOwner? = mock()

    // The observer of the event under test
    private val mEventObserver: Observer<Int> = mock()

    // Defines the Android Lifecycle of an object, used to trigger different events
    private var mLifecycle: LifecycleRegistry? = null

    // Event object under test
    private val mSingleLiveEvent = SingleLiveData<Int>()

    @Before
    @Throws(Exception::class)
    fun setUpLifecycles() {
        MockitoAnnotations.initMocks(this)

        // Link custom lifecycle owner with the lifecyle register.
        mLifecycle = LifecycleRegistry(mOwner!!)
        `when`(mOwner.lifecycle).thenReturn(mLifecycle)

        // Start observing
        mSingleLiveEvent.observe(mOwner, mEventObserver)

        // Start in a non-active state
        mLifecycle!!.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
    }

    @Test
    fun valueNotSet_onFirstOnResume() {
        // On resume
        mLifecycle!!.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)

        // no update should be emitted because no value has been set
        verify(mEventObserver, never()).onChanged(anyInt())
    }

    @Test
    fun singleUpdate_onSecondOnResume_updatesOnce() {
        // After a value is set
        mSingleLiveEvent.setValue(42)

        // observers are called once on resume
        mLifecycle!!.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)

        // on second resume, no update should be emitted.
        mLifecycle!!.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
        mLifecycle!!.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)

        // Check that the observer is called once
        verify(mEventObserver, times(1)).onChanged(anyInt())
    }

    @Test
    fun twoUpdates_updatesTwice() {
        // After a value is set
        mSingleLiveEvent.setValue(42)

        // observers are called once on resume
        mLifecycle!!.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)

        // when the value is set again, observers are called again.
        mSingleLiveEvent.setValue(23)

        // Check that the observer has been called twice
        verify(mEventObserver, times(2)).onChanged(anyInt())
    }

    @Test
    fun twoUpdates_noUpdateUntilActive() {
        // Set a value
        mSingleLiveEvent.setValue(42)

        // which doesn't emit a change
        verify(mEventObserver, never()).onChanged(42)

        // and set it again
        mSingleLiveEvent.setValue(42)

        // observers are called once on resume.
        mLifecycle!!.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)

        // Check that the observer is called only once
        verify(mEventObserver, times(1)).onChanged(anyInt())
    }
}