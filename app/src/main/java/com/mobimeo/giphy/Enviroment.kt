package com.mobimeo.giphy

interface EnvironmentType {
    val giphy: GiphyEnvironment
}
object Environment: EnvironmentType {
    override val giphy: GiphyEnvironment
        get() = GiphyEnvironment("https://api.giphy.com/v1/gifs/")
}

data class GiphyEnvironment(val url: String)