package com.mobimeo.giphy

import android.app.Application
import com.mobimeo.giphy.dependencies.myModule
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import org.koin.android.ext.android.startKoin

class GiphyApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(applicationContext, listOf(myModule))
        setupLeakCanary()
    }

    private fun setupLeakCanary(): RefWatcher {
        return if (LeakCanary.isInAnalyzerProcess(this)) {
            RefWatcher.DISABLED
        } else LeakCanary.install(this)
    }
}
