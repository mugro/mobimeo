package com.mobimeo.giphy.dependencies

import android.content.res.Resources
import com.bumptech.glide.Glide
import com.mobimeo.giphy.Environment
import com.mobimeo.giphy.extension.loadGif
import com.mobimeo.giphy.feature.FlowViewModel
import com.mobimeo.giphy.feature.base.BaseParams
import com.mobimeo.giphy.feature.giphy.GiphyParams
import com.mobimeo.giphy.feature.giphy.GiphyViewModel
import com.mobimeo.giphy.feature.giphy_list.GiphyListActions
import com.mobimeo.giphy.feature.giphy_list.GiphyListUseCase
import com.mobimeo.giphy.feature.giphy_list.GiphyListUseCaseType
import com.mobimeo.giphy.feature.giphy_list.GiphyListViewModel
import com.mobimeo.giphy.network.NetworkFactory
import com.mobimeo.giphy.network.api.ApiFactory
import com.mobimeo.giphy.network.api.ApiFactoryType
import com.mobimeo.giphy.repository.GiphyRepository
import com.mobimeo.giphy.repository.GiphyRepositoryType
import com.mobimeo.giphy.utils.AndroidSchedulerProvider
import com.mobimeo.giphy.utils.Screen
import com.mobimeo.giphy.utils.SchedulerProvider
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val myModule = module {

    single { Resources.getSystem() }

    single { createMoshi() }

    single { Screen(get()) }

    single<ApiFactoryType> { ApiFactory(get()) }

    single { NetworkFactory(get(), Environment.giphy) }

    single<SchedulerProvider> { AndroidSchedulerProvider() }

    single { get<NetworkFactory>().giphyApi }

    single<GiphyRepositoryType> { GiphyRepository(get(), get()) }

    factory<GiphyListUseCaseType>{ GiphyListUseCase(get()) }

    single { Glide.with(androidContext()).loadGif() }

    viewModel { (actions: GiphyListActions) -> GiphyListViewModel(get(), get(), actions) }

    viewModel { (initParams: GiphyParams) -> GiphyViewModel(initParams) }

    viewModel { FlowViewModel() }
}

private fun createMoshi(): Moshi {
    return Moshi.Builder().apply {
        add(KotlinJsonAdapterFactory())
    }.build()
}