package com.mobimeo.giphy.feature.giphy_list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mobimeo.giphy.R
import com.mobimeo.giphy.utils.ImageLoader
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_giphy.*

abstract class GiphyViewHolder<T: GiphyRow>(override val containerView: View)
    : RecyclerView.ViewHolder(containerView), LayoutContainer {

    abstract fun bind(item: T, imageLoader: ImageLoader)

    class Loading(itemView: View): GiphyViewHolder<GiphyRow.Loading>(itemView) {

        companion object {
            const val layoutId = R.layout.row_loader
        }

        override fun bind(item: GiphyRow.Loading, imageLoader: ImageLoader) {}
    }

    class Gif(itemView: View): GiphyViewHolder<GiphyRow.Giphy>(itemView) {

        companion object {
            const val layoutId = R.layout.row_giphy
        }

        override fun bind(item: GiphyRow.Giphy, imageLoader: ImageLoader) {
            imageLoader.load(item.url, R.color.colorPrimary, gifImage)
        }
    }
}