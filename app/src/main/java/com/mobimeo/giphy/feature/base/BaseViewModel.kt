package com.mobimeo.giphy.feature.base

import android.os.Bundle
import android.os.Parcelable
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.mobimeo.giphy.utils.MutableSecureLiveData
import com.mobimeo.giphy.utils.SecureLiveData
import com.mobimeo.giphy.utils.SingleLiveData
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.jetbrains.annotations.TestOnly

private const val KEY_VIEW_DATA = "KEY_VIEW_DATA"

abstract class BaseViewModel<T: Any, S: Any> : ViewModel() {

    abstract val initialViewData: T

    private val secureLiveData: SecureLiveData<T> by lazy { MutableSecureLiveData(initialViewData) }
    private val singleLiveData = SingleLiveData<S>()
    private val compositeDisposable = CompositeDisposable()

    protected fun updateViewData(newViewData: T, forceUpdate : Boolean = false) {
        if (newViewData != secureLiveData.value || forceUpdate) {
            (secureLiveData as MutableSecureLiveData<T>).value = newViewData
        }
    }

    protected open fun <A> updateViewData(forceUpdate: Boolean = false, newViewData: (A) -> T) {
        @Suppress("UNCHECKED_CAST")
        updateViewData(newViewData(viewDataValue() as A), forceUpdate)
    }

    protected fun updateSingleData(newSingleData: S) {
        singleLiveData.value = newSingleData
    }

    fun viewDataValue(): T = secureLiveData.value

    fun viewSingleValue(): S? = singleLiveData.value


    fun observeSecureData(owner: LifecycleOwner, observer: Observer<T>) = secureLiveData.observe(owner, observer)

    fun observeSingleData(owner: LifecycleOwner, observer: Observer<S>) = singleLiveData.observe(owner, observer)

    fun removeSecureDataObserver(observer: Observer<T>) {
        secureLiveData.removeObserver(observer)
    }

    fun removeSingleDataObserver(observer: Observer<S>) {
        singleLiveData.removeObserver(observer)
    }

    open fun onSave(outBundle: Bundle) {
        if (secureLiveData.value is Parcelable) {
            outBundle.putParcelable(KEY_VIEW_DATA, secureLiveData.value as Parcelable)
        }
    }

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }


    open fun onRestore(inBundle: Bundle?) {
        inBundle?.let {
            val parcelable = it.getParcelable<Parcelable>(KEY_VIEW_DATA)
            parcelable?.let { parcel ->
                @Suppress("UNCHECKED_CAST")
                updateViewData(parcel as T)
            }
        }
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    @TestOnly
    fun observeViewDataForever(observer: Observer<T>) = secureLiveData.observeForever(observer)

    @TestOnly
    fun observeSingleDataForever(observer: Observer<S>) = singleLiveData.observeForever(observer)

}