package com.mobimeo.giphy.feature

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.mobimeo.giphy.R
import com.mobimeo.giphy.feature.giphy.GiphyFragment
import com.mobimeo.giphy.feature.giphy.GiphyParams
import com.mobimeo.giphy.feature.giphy_list.GiphyListFragment
import com.mobimeo.giphy.feature.giphy_list.GiphyListParams
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    val viewModel by viewModel<FlowViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        viewModel.observeSingleData(this, Observer {
            if (it.params is ExitParams) {
                finish()
            } else {
                addNewFragment(it, createFragment(it))
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        viewModel.onBack()
    }

    private fun addNewFragment(viewData: FlowData, newFragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContainer, newFragment)
            .addToBackStack(viewData.id)
            .commit()
    }

    private fun createFragment(viewData: FlowData): Fragment {
        val params = viewData.params
        return when (params) {
            is GiphyListParams -> GiphyListFragment.CREATOR.newInstance(params)
            is GiphyParams -> GiphyFragment.CREATOR.newInstance(params)
            else -> {
                throw IllegalStateException("Unhandled view data type: " + viewData.javaClass.simpleName + " Did you forget to show the fragment?")
            }
        }
    }
}
