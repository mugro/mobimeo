package com.mobimeo.giphy.feature.giphy_list

sealed class GiphyRow {
    abstract val id: String

    data class Loading(override val id: String = "ID"): GiphyRow()

    data class Giphy(
        override val id: String,
        val url: String,
        val originalUrl: String
    ): GiphyRow()
}