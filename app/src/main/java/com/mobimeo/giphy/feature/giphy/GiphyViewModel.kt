package com.mobimeo.giphy.feature.giphy

import androidx.annotation.IntegerRes
import com.mobimeo.giphy.feature.base.BaseParams
import com.mobimeo.giphy.feature.base.BaseViewModel
import kotlinx.android.parcel.Parcelize

class GiphyViewModel(initParams: GiphyParams): BaseViewModel<GiphyData, @IntegerRes Int>() {

    override val initialViewData: GiphyData

    init {
        initialViewData = GiphyData.Gif(initParams.url)
    }
}


//ViewData
sealed class GiphyData {
    data class Gif(val url: String): GiphyData()
}

@Parcelize
class GiphyParams(val url: String): BaseParams