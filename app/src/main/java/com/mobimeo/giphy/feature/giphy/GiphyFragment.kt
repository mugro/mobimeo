package com.mobimeo.giphy.feature.giphy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IntegerRes
import com.mobimeo.giphy.R
import com.mobimeo.giphy.feature.base.BaseFragment
import com.mobimeo.giphy.feature.base.FragmentCreator
import com.mobimeo.giphy.utils.ImageLoader
import kotlinx.android.synthetic.main.fragment_giphy.*
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class GiphyFragment: BaseFragment<GiphyParams, GiphyData, @IntegerRes Int, GiphyViewModel>() {

    companion object {
        val CREATOR = FragmentCreator(GiphyFragment::class.java)
    }

    override val viewModel by viewModel<GiphyViewModel> { parametersOf(initParams) }
    private val imageLoader: ImageLoader = get()

    override fun applyViewData(viewData: GiphyData) {
        when(viewData) {
            is  GiphyData.Gif -> {
                imageLoader.load(viewData.url, R.color.colorPrimary, imageGif)
            }
        }
    }

    override fun applySingleData(singleData: Int) {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_giphy, container, false)
    }

}