package com.mobimeo.giphy.feature.giphy_list

data class ImageGif(
    val id: String,
    val fixedUrl: String,
    val originalUrl: String
)