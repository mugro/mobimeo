package com.mobimeo.giphy.feature

import com.mobimeo.giphy.feature.base.BaseParams
import com.mobimeo.giphy.feature.base.BaseViewModel
import com.mobimeo.giphy.feature.giphy.GiphyParams
import com.mobimeo.giphy.feature.giphy_list.GiphyListActions
import com.mobimeo.giphy.feature.giphy_list.GiphyListParams
import kotlinx.android.parcel.Parcelize
import java.util.*

class FlowViewModel : BaseViewModel<String, FlowData>(), GiphyListActions {

    override val initialViewData = ""
    private val stack = Stack<FlowData>()

    init {
        push(GiphyListParams())
    }

    override fun onGiphyClicked(url: String) {
        push(GiphyParams(url))
    }

    fun onBack() {
        pop()
    }

    private fun push(data: BaseParams) {
        stack.push(FlowData(data, data.toString()))
        updateSingleData(stack.peek())
    }

    private fun pop() {
        stack.pop()
        if(stack.empty()) {
            updateSingleData(FlowData(ExitParams(), "NO_ID"))
        }

    }
}

data class FlowData(
    val params: BaseParams,
    val id: String
)

@Parcelize
class ExitParams: BaseParams