package com.mobimeo.giphy.feature.giphy_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.recyclerview.widget.GridLayoutManager
import com.mobimeo.giphy.R
import com.mobimeo.giphy.feature.FlowViewModel
import com.mobimeo.giphy.feature.MainActivity
import com.mobimeo.giphy.feature.base.BaseFragment
import com.mobimeo.giphy.feature.base.FragmentCreator
import com.mobimeo.giphy.utils.EndlessScroll
import com.mobimeo.giphy.utils.Screen
import kotlinx.android.synthetic.main.fragment_giphy_list.*
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class GiphyListFragment : BaseFragment<GiphyListParams, GiphyListData, @StringRes Int, GiphyListViewModel>() {

    companion object {
        val CREATOR = FragmentCreator(GiphyListFragment::class.java)
    }

    private val flowViewModel by sharedViewModel<FlowViewModel>()
    override val viewModel by viewModel<GiphyListViewModel> { parametersOf(flowViewModel) }

    private val adapter: GiphyListAdapter by lazy {
        GiphyListAdapter(
            imageLoader = get(),
            listener = { viewModel.onGiphyClicked(it) }
        )
    }
    private lateinit var endlessScroll: EndlessScroll
    private val screen: Screen = get()

    override fun applyViewData(viewData: GiphyListData) {
        when (viewData) {
            GiphyListData.Loading -> {
                loaderView.visibility = View.VISIBLE
            }

            is GiphyListData.Gif -> {
                loaderView.visibility = View.GONE

                endlessScroll.enable(viewData.enableEndLessScroll)
                adapter.submitList(viewData.gifs.toMutableList())
            }
        }
    }

    override fun applySingleData(singleData: Int) {
        Toast.makeText(context, singleData, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_giphy_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gridLayoutManager = GridLayoutManager(context, screen.noOfColumns(90))
        endlessScroll = EndlessScroll(gridLayoutManager) { viewModel.requestGifs() }
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = adapter
        recyclerView.addOnScrollListener(endlessScroll)

        toolbar.title = getString(R.string.app_name)
    }
}