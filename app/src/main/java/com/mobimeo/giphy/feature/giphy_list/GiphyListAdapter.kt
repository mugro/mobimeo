package com.mobimeo.giphy.feature.giphy_list

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.mobimeo.giphy.extension.inflate
import com.mobimeo.giphy.utils.ImageLoader

class GiphyListAdapter(
    private val imageLoader: ImageLoader,
    private val listener: (String) -> Unit)
    : ListAdapter<GiphyRow, GiphyViewHolder<GiphyRow>>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GiphyRow>() {
            override fun areItemsTheSame(oldItem: GiphyRow, newItem: GiphyRow): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: GiphyRow, newItem: GiphyRow): Boolean {
                return oldItem == newItem
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiphyViewHolder<GiphyRow> {
        val view = parent.inflate(viewType)
        return when (viewType) {
            GiphyViewHolder.Loading.layoutId -> GiphyViewHolder.Loading(view)
            GiphyViewHolder.Gif.layoutId -> GiphyViewHolder.Gif(view).apply {
                view.setOnClickListener {
                    listener((getItem(adapterPosition) as GiphyRow.Giphy).originalUrl)
                }
            }
            else -> {
                throw IllegalArgumentException("View type not supported")
            }
        } as GiphyViewHolder<GiphyRow>
    }

    override fun onBindViewHolder(holder: GiphyViewHolder<GiphyRow>, position: Int) {
        holder.bind(getItem(position), imageLoader)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is GiphyRow.Loading -> GiphyViewHolder.Loading.layoutId
            is GiphyRow.Giphy -> GiphyViewHolder.Gif.layoutId
        }
    }
}