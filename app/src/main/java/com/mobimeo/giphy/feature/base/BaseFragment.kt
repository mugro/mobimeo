package com.mobimeo.giphy.feature.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer

class FragmentCreator<P : BaseParams, out F : BaseFragment<P, *, *, *>>(private val fragmentClass: Class<F>) {
    fun newInstance(params: P): F {
        return fragmentClass.newInstance().apply {
            initParams = params
        }
    }
}

abstract class BaseFragment<P: BaseParams ,D: Any, S: Any, VM: BaseViewModel<D, S>> : Fragment() {

    companion object {
        private const val INIT_PARAMS = "INIT_PARAMS"
    }

    protected abstract fun applyViewData(viewData: D)
    protected abstract fun applySingleData(singleData: S)

    protected abstract val viewModel: VM
    private lateinit var observer: Observer<D>
    private lateinit var singleObserver: Observer<S>
    var initParams: P
        get() = arguments!!.getParcelable(INIT_PARAMS)!!
        set(value) {
            val bundle = Bundle()
            bundle.putParcelable(INIT_PARAMS, value)
            arguments = bundle
        }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.onRestore(savedInstanceState)

        this.observer = Observer { applyViewData(it) }
        this.singleObserver = Observer { applySingleData(it) }

        viewModel.observeSecureData(viewLifecycleOwner, observer)
        viewModel.observeSingleData(viewLifecycleOwner, singleObserver)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.onSave(outState)
    }

    override fun onDestroyView() {
        viewModel.removeSecureDataObserver(observer)
        viewModel.removeSingleDataObserver(singleObserver)
        super.onDestroyView()
    }
}