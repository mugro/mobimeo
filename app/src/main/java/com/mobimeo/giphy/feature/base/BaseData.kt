package com.mobimeo.giphy.feature.base

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

interface BaseParams : Parcelable

@Parcelize
class EmptyParams: BaseParams