package com.mobimeo.giphy.feature.giphy_list

import androidx.annotation.StringRes
import com.mobimeo.giphy.R
import com.mobimeo.giphy.extension.convert
import com.mobimeo.giphy.feature.base.BaseParams
import com.mobimeo.giphy.feature.base.BaseViewModel
import com.mobimeo.giphy.feature.base.EmptyParams
import com.mobimeo.giphy.utils.SchedulerProvider
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.parcel.Parcelize
import org.jetbrains.annotations.TestOnly

interface GiphyListActions {
    fun onGiphyClicked(url: String)
}

class GiphyListViewModel(private val useCase: GiphyListUseCaseType,
                         private val schedulerProvider: SchedulerProvider,
                         private val actions: GiphyListActions
): BaseViewModel<GiphyListData, @StringRes Int>() {
    override val initialViewData = GiphyListData.Loading

    private val loadingRow = GiphyRow.Loading()

    private val gifs = mutableListOf<GiphyRow>()
    private var isFetched = true

    init {
        requestGifs()
    }

    fun requestGifs() {
        if(viewDataValue() is GiphyListData.Gif) {
            gifs.add(loadingRow)
            isFetched = false
            updateData()
        }

        addDisposable(useCase.getGifs()
            .observeOn(schedulerProvider.ui())
            .subscribeBy(
                onSuccess = {
                    isFetched = it.isNotEmpty()
                    gifs.remove(loadingRow)
                    gifs.addAll(it.convert())

                    updateData()
                },
                onError = {
                    isFetched = true
                    gifs.remove(loadingRow)

                    updateData()
                    updateSingleData(R.string.general_error)
                }
            ))
    }

    fun onGiphyClicked(originalUrl: String) {
        actions.onGiphyClicked(originalUrl)
    }

    private fun updateData() {
        updateViewData(GiphyListData.Gif(
            gifs = gifs.toList(),
            enableEndLessScroll = isFetched))
    }
}

//ViewData
sealed class GiphyListData {
    object Loading: GiphyListData()

    data class Gif(
        val gifs: List<GiphyRow>,
        val enableEndLessScroll: Boolean
    ): GiphyListData()
}

@Parcelize
class GiphyListParams: BaseParams