package com.mobimeo.giphy.feature.giphy_list

import com.mobimeo.giphy.repository.GiphyReponse
import com.mobimeo.giphy.repository.GiphyRepositoryType
import io.reactivex.Single
import org.jetbrains.annotations.TestOnly

class GiphyListUseCase(private val giphyRepositoryType: GiphyRepositoryType): GiphyListUseCaseType {

    companion object {
        const val MAX_PAGE = 20
        const val ITEMS_PAGE = 25
    }

    private var maxItems = MAX_PAGE * ITEMS_PAGE
    private var currentItems = 0

    override fun getGifs(): Single<List<ImageGif>> {
        return if(currentItems <= maxItems) {
            giphyRepositoryType.getGifs(currentItems, ITEMS_PAGE)
                .map { response: GiphyReponse ->

                    val totalItems = response.info.totalCount / ITEMS_PAGE
                    maxItems = if (totalItems > maxItems) maxItems else totalItems
                    currentItems += response.info.count

                    val imageGifList = mutableListOf<ImageGif>()
                    response.data.forEach {
                        imageGifList.add(ImageGif(
                            id = it.id,
                            fixedUrl = it.images.fixWidth.url,
                            originalUrl = it.images.original.url))
                    }

                    imageGifList
                }
        } else {
            Single.just(emptyList())
        }
    }

    @TestOnly
    fun setCurrentItems(totalItems: Int) {
        currentItems = totalItems
    }
}

interface GiphyListUseCaseType {
    fun getGifs(): Single<List<ImageGif>>
}