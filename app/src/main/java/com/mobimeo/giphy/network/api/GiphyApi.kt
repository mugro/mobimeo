package com.mobimeo.giphy.network.api

import com.mobimeo.giphy.repository.GiphyReponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GiphyApi {

    @GET("trending")
    fun getGifs(
        @Query("api_key") apiKey: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Single<GiphyReponse>
}