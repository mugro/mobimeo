package com.mobimeo.giphy.network

import com.mobimeo.giphy.GiphyEnvironment
import com.mobimeo.giphy.network.api.ApiFactoryType
import com.mobimeo.giphy.network.api.GiphyApi

class NetworkFactory(apiFactory: ApiFactoryType, giphyEnvironment: GiphyEnvironment) {

    val giphyApi: GiphyApi by lazy {
        apiFactory.createApi(GiphyApi::class.java, giphyEnvironment.url)
    }
}