package com.mobimeo.giphy.network.api

import com.mobimeo.giphy.BuildConfig
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class ApiFactory(moshi: Moshi): ApiFactoryType {

    private val retrofitBuilder: Retrofit.Builder

    init {
        val baseRetrofitBuilder = Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

        val level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        val loggingInterceptor = HttpLoggingInterceptor().setLevel(level)

        val httpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
        retrofitBuilder = baseRetrofitBuilder.client(httpClient)
    }

    override fun <T> createApi(apiClass: Class<T>, baseUrl: String): T {
        return retrofitBuilder.baseUrl(baseUrl).build().create(apiClass)
    }
}
