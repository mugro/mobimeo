package com.mobimeo.giphy.network.api

interface ApiFactoryType {
    fun <T> createApi(apiClass: Class<T>, baseUrl: String): T
}
