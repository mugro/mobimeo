package com.mobimeo.giphy.repository

import io.reactivex.Single

interface GiphyRepositoryType {
    fun getGifs(offset: Int, limit: Int): Single<GiphyReponse>
}