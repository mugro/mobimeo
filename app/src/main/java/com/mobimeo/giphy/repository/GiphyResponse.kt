package com.mobimeo.giphy.repository

import com.squareup.moshi.Json

data class GiphyReponse(
    @Json(name = "data") val data: List<Data>,
    @Json(name = "pagination") val info: Info,
    @Json(name = "meta") val meta: MetaData) {

    data class Data(
        @Json(name = "type") val type: String,
        @Json(name = "id") val id: String,
        @Json(name = "images") val images: Images
    ) {
        data class Images(
            @Json(name = "fixed_width") val fixWidth: Image,
            @Json(name = "original") val original: Image
        ) {
            data class Image(@Json(name = "url") val url: String)
        }
    }

    data class Info(
        @Json(name = "total_count") val totalCount: Int,
        @Json(name = "count") val count: Int,
        @Json(name = "offset") val offset: Int
    )

    data class MetaData(
        @Json(name = "status") val status: Int,
        @Json(name = "msg") val msg: String,
        @Json(name = "response_id") val responseId: String
    )
}


