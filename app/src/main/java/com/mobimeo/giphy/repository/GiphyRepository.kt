package com.mobimeo.giphy.repository

import com.mobimeo.giphy.BuildConfig
import com.mobimeo.giphy.network.api.GiphyApi
import com.mobimeo.giphy.utils.SchedulerProvider
import io.reactivex.Single

class GiphyRepository(private val giphyApi: GiphyApi,
                      private val schedulerProvider: SchedulerProvider
): GiphyRepositoryType {

    override fun getGifs(offset: Int, limit: Int): Single<GiphyReponse> {
        return giphyApi.getGifs(
            apiKey = BuildConfig.API_KEY,
            offset = offset,
            limit = limit)
            .subscribeOn(schedulerProvider.io())
    }
}