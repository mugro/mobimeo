package com.mobimeo.giphy.extension

import android.annotation.SuppressLint
import android.widget.ImageView
import com.bumptech.glide.RequestManager
import com.mobimeo.giphy.utils.ImageLoader
import com.bumptech.glide.request.RequestOptions

fun RequestManager.loadGif() = object : ImageLoader {
    @SuppressLint("CheckResult")
    override fun load(path: String, placeholder: Int, target: ImageView) {
        asGif()
            .apply(RequestOptions().placeholder(placeholder))
            .load(path)
            .into(target)


    }
}