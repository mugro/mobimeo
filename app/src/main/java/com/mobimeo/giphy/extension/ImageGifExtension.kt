package com.mobimeo.giphy.extension

import com.mobimeo.giphy.feature.giphy_list.GiphyRow
import com.mobimeo.giphy.feature.giphy_list.ImageGif

fun ImageGif.convert(): GiphyRow.Giphy {
    return GiphyRow.Giphy(
        id = id,
        url = fixedUrl,
        originalUrl = originalUrl
    )
}

fun List<ImageGif>.convert(): List<GiphyRow.Giphy> {
    val temp = mutableListOf<GiphyRow.Giphy>()
    forEach { temp.add(it.convert()) }
    return temp
}