package com.mobimeo.giphy.extension

import com.mobimeo.giphy.utils.SchedulerProvider
import io.reactivex.Single

fun <T> Single<T>.androidThreading(scheduler: SchedulerProvider): Single<T> = this
    .subscribeOn(scheduler.io())
    .observeOn(scheduler.ui())
