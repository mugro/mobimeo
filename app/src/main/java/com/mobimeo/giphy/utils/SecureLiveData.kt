package com.mobimeo.giphy.utils

import androidx.lifecycle.LiveData

open class SecureLiveData<T>(private val initialValue: T) : LiveData<T>() {

    init {
        value = initialValue
    }

    override fun getValue(): T {
        val value = super.getValue()
        if (value == null) logE("SecureLiveData value in NULL")
        return value ?: initialValue
    }
}

class MutableSecureLiveData<T: Any>(initialValue: T) :  SecureLiveData<T>(initialValue) {

    public override fun postValue(value: T) {
        super.postValue(value)
    }

    public override fun setValue(value: T) {
        super.setValue(value)
    }
}