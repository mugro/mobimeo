package com.mobimeo.giphy.utils

import android.widget.ImageView
import androidx.annotation.DrawableRes

interface ImageLoader {
    fun load(path: String,
             @DrawableRes placeholder: Int,
             target: ImageView)

}