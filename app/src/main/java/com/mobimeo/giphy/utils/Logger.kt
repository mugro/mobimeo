package com.mobimeo.giphy.utils

import android.util.Log
import com.mobimeo.giphy.BuildConfig

fun Any.logD(message: String, throwable: Throwable? = null) =  Log.d(this.javaClass.simpleName, message, throwable)

fun Any.logW(message: String, throwable: Throwable? = null) =  Log.w(this.javaClass.simpleName, message, throwable)


fun Any.logE(message: String, throwable: Throwable? = null) {
    Log.d(this.javaClass.simpleName, message, throwable)
    if (BuildConfig.DEBUG) {
        throw IllegalStateException(message, throwable)
    } else {
        //TODO: Sends error to crashlytics
    }
}
