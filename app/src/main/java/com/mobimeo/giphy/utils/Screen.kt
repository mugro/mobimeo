package com.mobimeo.giphy.utils

import android.content.res.Resources

class Screen(private val resources: Resources) {

    fun noOfColumns(columnWidthDp: Int): Int {
        val displayMetrics = resources.displayMetrics
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        return (dpWidth / columnWidthDp).toInt()
    }
}