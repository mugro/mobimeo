package com.mobimeo.giphy.utils

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class EndlessScroll(private val grigLayoutManager: GridLayoutManager,
                    private val function: () -> Unit): RecyclerView.OnScrollListener() {

    companion object {
        const val VISIBLE_THRESHOLD = 5
    }

    private var enable = true

    fun enable(isEnabled: Boolean) {
        enable = isEnabled
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val visibleItemCount = grigLayoutManager.childCount
        val totalItemCount = grigLayoutManager.itemCount
        val pastVisibleItems = grigLayoutManager.findFirstVisibleItemPosition()

        if (visibleItemCount + pastVisibleItems + VISIBLE_THRESHOLD >= totalItemCount && enable) {
            enable = false
            function.invoke()
        }
    }
}